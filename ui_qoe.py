import os
import dpkt
import datetime
import pickle
import sys
import traceback

import gesture_processor

from ui_session import ui_session

def initialize_flows_on_sessions():
    sessions = pickle.load(open("sessions.p", "rb"))

    # Clear all flow associations.
    # This function's job is to rebuild them.
    for ses in sessions:
	ses.flows = []

    pickle.dump(sessions, open("sessions.p", "wb"))  

def process_ui(session):
    try:
        ui_file_ref = session.ui_data[0]
    
        # TRIPLE TAP DETECTION.
        triple_tap_gesture = gesture_processor.repeated_tap_gesture(3, 30, 2)
        session.detected_triple_tap_gesture = gesture_processor.contains_gesture(triple_tap_gesture, ui_file_ref)

        # QUAD-POPULARITY DETECTION.
        quad_popularity_gesture = gesture_processor.quadrant_popularity(720, 1280)
        gesture_processor.contains_gesture(quad_popularity_gesture, ui_file_ref)    
        if all(x == quad_popularity_gesture.quad_times[0] for x in quad_popularity_gesture.quad_times):
	   session.popular_quadrant = -1
        else:
	   session.popular_quadrant = quad_popularity_gesture.quad_times.index(max(quad_popularity_gesture.quad_times))
	# LONG-PRESS DETECTION
	long_press_gesture = gesture_processor.long_press_gesture(30, 1)
	session.detected_long_press_gesture = None
	session.detected_long_press_gesture = gesture_processor.contains_gesture(long_press_gesture, ui_file_ref)

    except Exception as details:
	raise "ERROR DETECTED: " + str(details) 

#def sort_flows(flow_lines):
#    for line in flow_lines:
#	tokens = line.split() 
#	start_time = tokens[11]
#	if float(start_time) < 0:
#	    start_time = tokens[12]
#	start_times.append(start_time)
	
    

def process_networking(sessions, flow_lines, app_filter):
    # Clear all flow associations.
    # This function's job is to rebuild them.

    	
    print "session num flows: " + str(len(session.flows))
    print "done processing networking"

def session_from_ui_file(file_ref):
    try:
	with open(file_ref, "r") as f_ui_file:
	    #print "...done!"
	    ui_file_data = f_ui_file.read()
	    ui_events = ui_file_data.split(os.linesep)
	    
	    for m in [s for s in ui_events if "Start" in s or "Finish" in s or s == "" or s == " "]:
		ui_events.remove(m) 
 
	    if len(ui_events) <= 0:
		return None

	    # Grab timestamps
	    ui_events_begin_timestamp = ui_events[0][:17]
	    ui_events_end_timestamp = ui_events[-1][:17]

	    # get user_id.
	    found_user_id = file_ref[84:99]

	    new_session = ui_session()
	    new_session.start_time = ui_events_begin_timestamp
	    new_session.end_time = ui_events_end_timestamp		    			
	    new_session.user_id = found_user_id
	    new_session.ui_data.append(file_ref)

	    # Verify sessions attributes are valid.
	    try:
		float(new_session.start_time)
		float(new_session.end_time)
		float(new_session.user_id)
	    except Exception as details:
		print "ERROR: " + str(details)
		
	    return new_session

    except Exception as details:
	print "ERROR: " + str(details)
	print "FAILED to process: " + file_ref

    return None

def calculate_gestures(sessions):
    print "Calculating gestures..."
    sessions_to_delete = []
    num_sessions = len(sessions)
    print "num sessions to examine: " + str(num_sessions)
    for i in range(num_sessions):
	if i % 1000 == 0:
	    os.system("clear")
	    print "Calcularing Gestures...\n" + str(i) + " / " + str(num_sessions)
	try:
	    prospective_new_session = sessions[i]
	    process_ui(prospective_new_session)
	    if prospective_new_session.popular_quadrant == -1 and i not in sessions_to_delete:
		sessions_to_delete.append(i)

	except:
	    sessions_to_delete.append(i)

    print "...Removing bad sessions..."
    for i in reversed(sessions_to_delete):
	del sessions[i]
    print "...done!" 

def dump_array(arr, file_name):
    pickle.dump(arr, open(file_name, "wb"))  

def load_array(arr, file_name):
    arr[:] = pickle.load(open(file_name, "rb"))

def create_sessions(sessions, max_session_time_length):
    with open('useful_ui_files', 'r') as f_ui:
	ui_location_data = f_ui.read() 
	ui_file_references = sorted(ui_location_data.split(os.linesep))	
	num_ui_file_references = len(ui_file_references)
	for i in range(num_ui_file_references):
	    ref = ui_file_references[i]
	    if i % 1000 == 0:
		os.system("clear")
		print "Generating sessions...\n" + str(i) + " / " + str(num_ui_file_references)
	    try:
		if len(ref) <= 0:
		    continue
		# Create session from UI.
		prospective_new_session = session_from_ui_file(ref)
		if prospective_new_session is None:
		    continue
		if float(prospective_new_session.end_time) - float(prospective_new_session.start_time) > max_session_time_length:
		    continue	

		sessions.append(prospective_new_session)
	    except:
		pass

def calculate_network(sessions, flow_lines, app_filter):
    print "BEGIN NET CALCULATE"
    sessions_to_delete = []
    num_sessions = len(sessions)
    num_lines = len(flow_lines)
    flows_found = 0
    current_line_num = 0

    for i in range(num_sessions):
	session = sessions[i]
	
	found_a_success = False

	del session.flows[:] 
	if i % 100 == 0:
	    #os.system("clear")
	    print "Adding network data...\n" + str(i) + " / " + str(num_sessions) + "\nFlows found: " + str(flows_found)
	try:
	    for j in range(max(0, current_line_num), num_lines):
		tokens = flow_lines[j]
	
		# OPT: begin at the final line to not work or be culled.
		# If working, should have 1782 flows by session 12000
	
		# For some reason, the data provides both a start_time and a tmp_start_time field.
		# Sometimes, one of the other will be -1.000...
		# When this happens, we should use the other one.
		start_time = tokens[11]
		if float(start_time) < 0:
		    start_time = tokens[12]
		app_name = tokens[8]
		end_time = tokens[24]
		user_id = tokens[0]
			
		if session.end_time < start_time:
		    break
		
		#if session.user_id == user_id:		
		    #print str(session.start_time) + " < " + str(start_time) + " : " + str(session.start_time < start_time) 
		    #print str(session.end_time) + " > " + str(end_time) + " : " + str(session.end_time > end_time)
		    #print ""
		# associate flow with session.
		#if session.start_time < start_time and session.end_time > end_time and session.user_id == user_id:
    
		#print "found tokens of size " + str(len(tokens))
		#print tokens
		#print "start time: " + str(tokens[11]) + " or " + str(tokens[12])
		#print "end time: " + str(end_time)
		#print "start time: " + str(start_time)
		#print "looking for: 1356142834.358462"		
		#if start_time == "1356142834.358462" and end_time == "1356142837.793408":
		    #print "HOLY FUCK"
		    #print("found at: " + str(previous_session_ind))
		    #print("flow start: " + str(start_time) + " flow end: " + str(end_time))
		    #print(ses)
		    #print ""

		if (session.start_time < start_time and session.end_time > start_time) or (session.start_time < end_time and session.end_time > end_time):
		    if session.user_id == user_id:
			session.flows.append(tokens)
			flows_found += 1
		    if not found_a_success:
			found_a_success = True
			current_line_num = j - 1000
		    #print "flows_found: " + str(flows_found)
	except Exception as details:
	    print details

    #for i in range(num_sessions):
	#session = sessions[i]
	#num_flows = len(session.flows)
	#if num_flows < min_flows or num_flows > max_flows:	
	#    sessions_to_delete.append(i)

    #print "...Removing bad sessions..."
    #for i in reversed(sessions_to_delete):
	#del sessions[i]
    print "...done!" 

def prepare_flows(flow_lines, app_filter):
    print "preparing flows..."
    lines_to_remove = []
    for i in range(len(flow_lines)):
	flow_lines[i] = flow_lines[i].split(' ')
	if len(flow_lines[i]) < 13:
	    lines_to_remove.append(i)
	else:
	    # Filter based on app name.
	    app_name = flow_lines[i][8]
	    if app_filter is not None:
		if not any(x in app_name for x in app_filter):
		    lines_to_remove.append(i)

    for i in reversed(lines_to_remove):
	del flow_lines[i]
    
    print "sorting flow lines"
    sorting_lambda = lambda x: x[12] if x[11] <= 0 else x[11]
    flow_lines.sort(key=sorting_lambda)
    print "...done!" 

def verify_flows_sorted(flow_lines):
    start_times = []
    for tokens in flow_lines:
		
	# For some reason, the data provides both a start_time and a tmp_start_time field.
	# Sometimes, one of the other will be -1.000...
	# When this happens, we should use the other one.
	start_time = tokens[11]
	if float(start_time) < 0:
	    start_time = tokens[12]
		
	start_times.append(start_time)
	
    print "ARE START TIMES ORDERED:"
    print sorted(start_times) == start_times	

    print "finding offender:"
    for i in range(0, 9):
	print start_times[i]
    	

def generate_sessions(sessions_output_file, max_session_time_length, min_flows, max_flows, app_filter, num_sessions_desired):
    sessions = []
        
    create_sessions(sessions, max_session_time_length)
    dump_array(sessions, sessions_output_file + "_POST_create.p")
    print "num sessions left: " + str(len(sessions))

    load_array(sessions, sessions_output_file + "_POST_create.p")	
    calculate_gestures(sessions)
    dump_array(sessions, sessions_output_file + "_POST_gestures.p")
    print "num sessions left: " + str(len(sessions))
    
    flow_lines = open('/z/user-study-imc15/PACO/flow_summary.txt', 'r').read().split(os.linesep)
    prepare_flows(flow_lines, app_filter)
    dump_array(flow_lines, sessions_output_file + "_FLOW_LINES")

    flow_lines = []
    load_array(flow_lines, sessions_output_file + "_FLOW_LINES")  

    print "sorting flow lines"
    for i in range(len(flow_lines)):
	if float(flow_lines[i][11]) < 0:
	    flow_lines[i][11] = flow_lines[i][12]

    print "showing lines: "
    for i in range(9):
	print flow_lines[i][11] + " " + flow_lines[i][12]
    sorting_lambda = lambda x: x[11]
    flow_lines.sort(key=sorting_lambda)
    print "...done!" 

    verify_flows_sorted(flow_lines)

    load_array(sessions, sessions_output_file + "_POST_gestures.p")
    sorting_lambda = lambda x: x.start_time
    sessions.sort(key=sorting_lambda)
    print "num flow lines: " + str(len(flow_lines))
    calculate_network(sessions, flow_lines, app_filter)
    dump_array(sessions, sessions_output_file + "_POST_network.p")
    print "num sessions left: " + str(len(sessions))		 

    filter_sessions(sessions_output_file + "_POST_network.p", sessions_output_file + "_POST_clean.p", max_session_time_length, min_flows, max_flows)

    write_sessions_to_text(sessions_output_file + "_POST_clean.p", sessions_output_file + "_POST_clean_stringified")
    print "NUM FLOW LINES"
    for session in sessions:
	print len(session.flows)   


    write_csv_file(sessions_output_file + "_POST_clean.p", sessions_output_file + "_triple_tap.csv", "triple_tap")
    write_csv_file(sessions_output_file + "_POST_clean.p", sessions_output_file + "_pop_quad.csv", "pop_quad")
    write_csv_file(sessions_output_file + "_POST_clean.p", sessions_output_file + "_long_press.csv", "long_press")

    #print "writing sessions..."
    #pickle.dump(sessions, open(sessions_output_file, "wb"))  
    #print "...done!"

def associate_sessions_with_network_activity(sessions_input_file, sessions_output_file):
    print "depickling..."
    sessions = pickle.load(open(sessions_input_file, "rb"))

    # Clear all flow associations.
    # This function's job is to rebuild them.
    for ses in sessions:
	del ses.flows[:]

    num = 0
    print "...done!"
    print "investigating flows..."
    with open('/z/user-study-imc15/PACO/flow_summary.txt', 'r') as f_flow:

	flow_data = f_flow.read() 
	flow_lines = flow_data.split(os.linesep)
	total_lines = len(flow_lines)
	print str(total_lines) + " lines to examine"

	num_found = 0
	previous_session_ind = 0		
	for line_num in range(0, len(flow_lines)):
	    line = flow_lines[line_num]
	    num += 1
	    if num % 10 == 0:
		print "FOUND: " + str(num_found)
		
	    
	    if num % 10000 == 0:
		 print(str(num) + '/' + str(total_lines))
	    #os.system("clear")
	    line = line.split(" ")
	    user_id = line[0]

	    # For some reason, the data provides both a start_time and a tmp_start_time field.
	    # Sometimes, one of the other will be -1.000...
	    # When this happens, we should use the other one.
	    start_time = line[11]
	    if float(start_time) < 0:
		start_time = line[12]
	    app_name = line[8]
	    end_time = line[24]
	    #if not "facebook" in app_name and not "fb" in app_name:
		#continue
	    #print "FOUND: " + app_name
	    for i in range(0, len(sessions)):
		ses = sessions[i]	
		
		#if start_time > ses.end_time:
		#    print ("start_time: " + start_time + " ses.end_time: " + ses.end_time)
		#    break

		if ses.start_time < start_time and ses.end_time > end_time and ses.user_id == user_id:
		    ses.flows.append(line)
		    previous_session_ind = i
		    num_found += 1
		    print("found at: " + str(previous_session_ind))
		    print("flow start: " + str(start_time) + " flow end: " + str(end_time))
		    print(ses)
		    print ""

    print "writing sessions..."
    #pickle.dump(sessions_output_file, open("sessions_100000.p", "wb"))  
    print "...done!"
    """
    with open('/z/user-study-imc15/PACO/pcaplistallsort_full', 'r') as f:
	pcap_file_location_data = f.read()
	pcap_file_references = pcap_file_location_data.split(os.linesep)
	num_pcap_directories = len(pcap_file_references)
	#print ("parsing " + str(num_pcap_directories) + " pcap files.")
	#print (pcap_file_references)
	
		
	num_pcap_directories_visited = 0
	for pcap_path in pcap_file_references:
	    num_pcap_directories_visited += 1
	    #os.system('clear')
	    print (str(num_pcap_directories_visited) + '/' + str(num_pcap_directories))

	    pcap_directory_location = os.path.dirname(pcap_path)
	
	    time_data = ""

	    # APPNAME DATA
	    print(pcap_directory_location + "/time")
	    return
	    with open(pcap_directory_location + "/time", 'r') as f_time:
		time_data = f_time.read()
		print "FUCK1"
		return
	    

	    # PROCESS TIME DATA
	    time_data_lines = time_data.split(os.linesep)
	    time_stamp = time_data_lines[1]
	    time_length = time_data_lines[2]
	   
	    print "FUCK"
	    return
	    # pcap file start time.
	    with open(pcap_path) as f_pcap:
		try:
		    pcap = dpkt.pcap.Reader(f_pcap)

		    beginning_timestamp = None
		    ending_timestamp = None

		    for timestamp, buf in pcap:
		        ending_timestamp = None
		        if beginning_timestamp == None:
			   beginning_timestamp = timestamp
			    
		    print "Scanning available sessions..."
		    for ses in sessions:
			print	"beginning_timestamp: {}, end_stamp: {}".format(beginning_timestamp, ending_timestamp)
		        if ses.start_time < beginning_timestamp and ses.end_time > ending_timestamp:
			   print "FOUND UI-NET MATCH:"
			   print ses
			   print timestamp
			   print ""
		except:
		    print "packet analysis error."
	    # new session
	    #new_session = session()
	    #new_session._start_time = time_stamp
	    #new_session._end_time = time_stamp + time_length
    
	    #first_4_timestamp_digits = time_stamp[:10]
	    #print(first_4_timestamp_digits)

	    # GRAB UI DATA
	   
	    #print("appname:\n" + appname_data)
	    #print("processed_events:\n" + processed_events_data)
	    #print("time:\n" + time_data)

    #list_files("/nfs/beirut1/userstudy/2nd_round/userinput/")
    #list_files("/nfs/beirut1/userstudy/2nd_round/userinput/")
    #list_files("/nfs/beirut1/userstudy/2nd_round/userinput/")
    """

def detect_gestures(sessions_file, sessions_output_file):
    sessions = pickle.load(open(sessions_file, "rb"))
    print "...done!"

    num_visited = 0
    num_positive_triple_tap = 0
    num_positive_long_press = 0
    num_sessions = len(sessions)

    errors_detected = 0

    for ses in sessions:
	try:
	    
	    ui_file_ref = ses.ui_data[0]
	
	    # TRIPLE TAP DETECTION.
	    #triple_tap_gesture = gesture_processor.repeated_tap_gesture(3, 30, 2)
	    #ses.detected_triple_tap_gesture = None
	    #ses.detected_triple_tap_gesture = gesture_processor.contains_gesture(triple_tap_gesture, ui_file_ref)
	    #num_positive_triple_tap += ses.detected_triple_tap_gesture

	    # QUAD-POPULARITY DETECTION.
	    #quad_popularity_gesture = gesture_processor.quadrant_popularity(1280, 720)
	    #ses.popular_quadrant = None
	    #gesture_processor.contains_gesture(quad_popularity_gesture, ui_file_ref)
	    
	    
	    #if all(x == quad_popularity_gesture.quad_times[0] for x in quad_popularity_gesture.quad_times):
		#ses.popular_quadrant = -1
	    #else:
		#ses.popular_quadrant = quad_popularity_gesture.quad_times.index(max(quad_popularity_gesture.quad_times))

	    
	    long_press_gesture = gesture_processor.long_press_gesture(30, 1)
	    ses.detected_long_press_gesture = None
	     
	    ses.detected_long_press_gesture = gesture_processor.contains_gesture(long_press_gesture, ui_file_ref)
	    num_positive_long_press += ses.detected_long_press_gesture
		
	except Exception as details:
	    print "ERROR DETECTED: "  + str(details)
	    errors_detected += 1
	
	num_visited += 1
	if num_visited % 1000 == 0 or num_visited == num_sessions:
	    print str(num_visited) + "/" + str(num_sessions)
	    print "positive rate for triple tap:" + str(float(num_positive_triple_tap) / float(num_visited))	
	    print "positive rate for long press:" + str(float(num_positive_long_press) / float(num_visited))
	    #print "current file: " + ui_file_ref
	    #print "Quad times: " + str(quad_popularity_gesture)
	
	
    print "writing sessions..."
    pickle.dump(sessions, open(sessions_output_file, "wb"))  
    print "...done!"
    print "Detect Gestures FINISHED"
    print "Errors Detected: " + str(errors_detected)
	
def write_csv_file(sessions_filename, output_filename, attribute):
    string_rep = ""
    print "depickling..."
    sessions = pickle.load(open(sessions_filename, "rb"))
    print "...done!"
    for ses in sessions:
	if attribute == "triple_tap":
	    if ses.detected_triple_tap_gesture == None:
		ses.detected_triple_tap_gesture = False
	    string_rep += str(int(ses.detected_triple_tap_gesture))

	elif attribute == "long_press":
	    if ses.detected_long_press_gesture == None:
		ses.detected_long_press_gesture = False
	    string_rep += str(int(ses.detected_long_press_gesture))

	elif attribute == "pop_quad":
	    if ses.popular_quadrant == None:
		ses.popular_quadrant = -1
	    string_rep += str(ses.popular_quadrant)
	
	string_rep += ','
 
    with open(output_filename, "wb") as f_output:
	f_output.write(string_rep)

def write_sessions_to_text(sessions_filename, output_filename):

    with open(output_filename, "w") as f_sessions:
	print "depickling..."
	sessions = pickle.load(open(sessions_filename, "rb"))
	print "...done!"
    
	for ses in sessions:
	    if len(ses.flows) == 0:
	        continue
	    print(ses)
	    f_sessions.write(str(ses))
	    try:
	        float(ses.start_time)
	        float(ses.end_time)
	        float(ses.user_id)
	    except:
	        print "ERROR WRITING TO TEXT."
	        return

def load_session_data():
    session_list = []
    f = open("/z/user-study-imc15/PACO/user_session_summary.txt")
    for line in f.readlines():
	(userid, start_time, end_time, active_time, session_id, discard_1, discard_2, num_clicks, is_session) = line.split()
	end_time = float(end_time)*1000
	start_time = float(start_time)*1000
	active_time = float(active_time)
	num_clicks = int(num_clicks)
	if is_session != "1" or start_time > end_time:
	    continue
	session_list.append((userid, start_time, end_time, active_time, session_id, num_clicks, is_session))

    f = open("output_files/session_summary_shitty.txt", "w")
    
    for session_info in session_list:
	f.write(str(session_info))

def print_sessions(sessions_filename):
    sessions = []
    load_array(sessions, sessions_filename)
    
    for session in sessions:
	print session.popular_quadrant
	print session.detected_long_press_gesture
	#for flow in session.flows:
	#    print len(flow)
	#    if len(flow) != 40:
#		print "SHIT"
#		return;

def filter_sessions(sessions_filename, sessions_output_file, max_session_time, min_flow_num, max_flow_num):
    sessions = [] 
    sessions_to_delete = []
    load_array(sessions, sessions_filename)

    for i in range(len(sessions)):
	current_session = sessions[i]
	diff = float(current_session.end_time) - float(current_session.start_time)
	num_flows = len(current_session.flows)
	
	# eliminate via time length
	if diff > max_session_time:
	    sessions_to_delete.append(i)
	# eliminate via flow count	
	if num_flows < min_flow_num or num_flows > max_flow_num and not i in sessions_to_delete:
	    sessions_to_delete.append(i)
	# eliminate via gestures.
	if current_session.popular_quadrant == -1 and not i in sessions_to_delete:
	    sessions_to_delete.append(i)

    print "...Removing bad sessions..."
    for i in reversed(sessions_to_delete):
	del sessions[i]
    print "...done!"
    dump_array(sessions, sessions_output_file)
    print "num sessions remaining: " + str(len(sessions))

#print_sessions("final_facebook_60_1_20_POST_gestures.p")
#filter_sessions("experiment_POST_network.p", "experiment_POST_clean.p", 20, 1, 10)
generate_sessions("candycrush_60_1_20", 60, 1, 20, ["candycrush"], 315743000)
#generate_sessions("operation_find_flows", 60, 1, 10, ["fb", "facebook"], 315743)
#create_sessions_from_ui("10000_facebook_sessions.p")
#associate_sessions_with_network_activity("experiment_POST_gestures.p", "wheee.p")
#detect_gestures("sessions_100000_with_triple_tap.p", "sessions_100000_with_triple_tap_and_pop_quad.p")
#write_sessions_to_text("experiment_POST_clean.p", "experiment_POST_clean_stringified")
#detect_gestures("sessions_100000.p", "sessions_100000_with_long_press.p")
#write_csv_file("experiment_POST_clean.p", "triple_tap_experiment_POST_clean.csv", "triple_tap")
#write_sessions_to_text("sessions_100000_with_triple_tap_and_pop_quad.p", "sessions_100000_with_triple_tap_and_pop_quad_stringified")
#detect_gestures("sessions_100000.p", "sessions_100000_with_long_press_2nd.p")
#write_csv_file("sessions_100000_with_triple_tap.p", "sessions_100000_with_triple_tap.csv", "triple_tap")
#list_files("")
#write_csv_file("experiment_POST_clean.p", "pop_quad_experiment_POST_clean.csv", "pop_quad")
#prepare_timestamp_ui_file_mapping()
#write_csv_file("sessions_100000_with_long_press.p", "sessions_100000_with_long_press.csv", "long_press")
#write_csv_file("sessions_100000_with_long_press_2nd.p", "sessions_100000_with_long_press_2nd.csv", "long_press")
