function [featVector_out] = computeStats(featMat, flow_struct)

if size(featMat, 1)==1
	featVector_out = featMat;
else
	featVector_out = nanmean(featMat);
end
