function [featVector_out] = getFeatures(rawDataVector, flow_struct)

% get indices of stuff to keep 
idx = [flow_struct{:,2}]'>0;

% clean raw data
rawDataVector(~idx) = [];

% get output (might need to do more pre-processing later)
featVector_out = str2double(rawDataVector)';


