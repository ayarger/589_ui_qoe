clear; clc; 

% define flow structure
flow_struct = {
	'userID',				0;
	'tcp_flows_size',	 	1;
	'unique_flow_id',	 	0;
	'clt_ip_tuple',	 		1;
	'server_ip_tuple',	 	1;
	'network_type',	 		1;
	'packet_count',	 		1;
	'app_packet_count',	 	1;
	'app_name',	 			0;
	'active_energy',	 	1;
	'passive_energy',	 	1;
	'tmp_start_time',	 	0;
	'fg_log',	 			0;
	'stuff_1',			 	0;
	'stuff_2',			 	0;
	'stuff_3',			 	0;
	'stuff_4',			 	0;
	'stuff_5',			 	0;
	'total_ul_payload',	 	1;
	'total_dl_payload',	 	1;
	'total_ul_whole',	  	1;
	'total_dl_whole',	 	1;
	'ul_time',	  			1;
	'dl_time',	  			1;
	'last_tcp_ts',	 		0;
	'total_ul_payload_h',	1;
	'total_dl_payload_h',	1;
	'ul_rate_payload',	  	1;
	'dl_rate_payload',	 	1;
	'ul_rate_payload_h',	1;
	'dl_rate_payload_h',	1;
	'http_request_count',	1;
	'timestamp_log',	 	0;
	'energy_log',	 		0;
	'content_type',	 		0;
	'user_agent',	 		0;
	'host',	 				0;
	'content_length',	 	0;
	'total_content_length',	1;
	'request_url',	 		0;
   };

% instantiate some variables
dim = sum([flow_struct{:,2}])+8;

% read input file
fileIDin = fopen('../data/sessions_100000_stringified');

% open output file
instancesPath = '../data/instances.txt';
labelsPath = '../data/labels.txt';

fprintf('...deleting existing output files\n')
delete(instancesPath)
delete(labelsPath)

% read text file
while ~feof(fileIDin)

	% read line
	tline = fgetl(fileIDin);

	% check if new session
	if strcmp(tline, 'start_time')

		fprintf('...servicing session\n')

		% get session length
		start_time = fgetl(fileIDin);
		fgetl(fileIDin);
		end_time = fgetl(fileIDin);

		% get user ID
		fgetl(fileIDin);		
		fgetl(fileIDin);

		% get label 
		fgetl(fileIDin);
		fgetl(fileIDin);

		% get number of flows
		tline = fgetl(fileIDin);
		numFlows = str2double(tline(6:end));
		fprintf('......flows: %d\n', numFlows)

		% get begin flow label
		tline = fgetl(fileIDin);
		
		assert(strcmp(tline, 'BEGIN FLOW'))

		% initialize feature matrix
		featMat = zeros(numFlows, dim);

		% populate each column of the matrix
		for j=1:numFlows

			% initialize cell to hold 40 lines
			flowLines = cell(length(flow_struct),1);

			% skip
			if (j>1)
				fgetl(fileIDin);
			end	

			% populate the cell
			for k=1:length(flow_struct)						
				tline = fgetl(fileIDin);
				flowLines{k} = tline;			
			end

			% get feature vector from flow
			[featMat(j,:), ~] = getFeaturesSession(flowLines, flow_struct);			
		end

		% compute statistics from all flows
		featVector = computeStatsSession(featMat, flow_struct);

		% output vector into file
		dlmwrite(instancesPath, featVector, '-append')

		% output label
		dlmwrite(labelsPath, rand>0.5, '-append')

	end
end

% close files
fclose(fileIDin);


