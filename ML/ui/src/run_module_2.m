clear; clc;

% read instances files
X = csvread('../data/instances.txt');
Y = csvread('../data/labels.txt');

% get sizes
[n, d] = size(X);

% get number of classes
uniqueClasses = unique(Y);
numClasses = length(uniqueClasses);

% print some information
fprintf('*** number of instances: %d\n', n);
fprintf('*** dimensionality: %d\n', d);
fprintf('*** number of classes: %d\n', numClasses);

% print class imbalance ratio
for i =1:numClasses	
	fprintf('*** class %d ratio: %0.2f\n', i, sum(Y==uniqueClasses(i))/length(Y));
end

% do rank-sum analysis
