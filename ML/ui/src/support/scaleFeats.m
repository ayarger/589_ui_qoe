function [Xtr_o, Xte_o] = scaleFeats(Xtr, Xte)

	% get sizes
	nTr = size(Xtr, 1);
	nTe = size(Xte, 1);

	% get dimension
	dim = size(Xte, 2);

	% combine both matrices
	X = [Xtr; Xte];

	% initialize scaled matrix
	XScaled = X;

	% for each feature
	for i=1:dim
		currFeats = X(:,i);

		% get max and min
		xmin = nanmin(currFeats);
		xmax = nanmax(currFeats);

		% sanity check
		assert(xmin~=xmax)

		% put into scaled matrix
		XScaled(:,i) = (currFeats-xmin)./(xmax-xmin);		
	end


	% get Xtr and Xte back
	Xtr_o = XScaled(1:nTr, :);
	Xte_o = XScaled(nTr+1:end, :);


end