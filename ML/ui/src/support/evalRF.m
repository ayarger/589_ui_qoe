function resultsOut = evalRF(Xtr, Ytr, Xte, Yte)

	% initialize results
	results = cell(1, 4);

	% train with optimal C
	model = TreeBagger(500, Xtr, Ytr);

	% predict
	[~, preds] = model.predict(Xte);

	% get positive predictions 
	posPreds = preds(:,(strcmp(model.ClassNames(1),'0')+1));

	% get positive values
	posPreds = preds(:,(strcmp(model.ClassNames(1),'0')+1));

	% compute AUROC
	[a,b,~,AUROC] = perfcurve(Yte, posPreds, 1);

	% check if < 0.5
	if AUROC<=0.5
		fprintf('****** flipping\n')
		[a,b,~,AUROC] = perfcurve(Yte, posPreds, 0);
	end

	% save everything	
	results{1} = AUROC;
	results{2} = a;
	results{3} = b;
	results{4} = model;

	% set output
	resultsOut = results;

end

