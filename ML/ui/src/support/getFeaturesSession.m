function [featVector_out, flow_struct_out] = getFeaturesSession(rawDataVector, flow_struct)

% get indices of stuff to keep 
idx = [flow_struct{:,2}]'>0;

% clean raw data
rawDataVector(~idx) = [];
flow_struct(~idx,:) = [];
flow_struct(:,2) = [];

% get IP features and remove them from vector
clt_IP_str = rawDataVector{ismember( flow_struct, 'clt_ip_tuple')};
server_IP_str = rawDataVector{ismember( flow_struct, 'server_ip_tuple')};

rawDataVector(ismember(flow_struct, 'clt_ip_tuple')) = [];
flow_struct(ismember(flow_struct, 'clt_ip_tuple')) = [];
rawDataVector(ismember(flow_struct, 'server_ip_tuple')) = [];
flow_struct(ismember(flow_struct, 'server_ip_tuple')) = [];

% get client port and IP
temp = strsplit(clt_IP_str,':');
clt_port = str2num(temp{2});
clt_IP = strsplit(temp{1},'.');
clt_IP_vec = zeros(1, length(clt_IP));
for i=1:length(clt_IP)
	clt_IP_vec(i) = str2num(clt_IP{i});
end

% get server port and IP
temp = strsplit(server_IP_str,':');
server_port = str2num(temp{2});
server_IP = strsplit(temp{1},'.');
server_IP_vec = zeros(1, length(server_IP));
for i=1:length(server_IP)
	server_IP_vec(i) = str2num(server_IP{i});
end

% get other 
otherFeats = str2double(rawDataVector)';

% set outputs
featVector_out = [clt_IP_vec, clt_port, server_IP_vec, server_port, otherFeats];
flow_struct_out = flow_struct;
