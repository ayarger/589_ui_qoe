function table = performRankSumTest(X, Y)

	fprintf('*** performing Rank Sum Tests\n')

	% get some numbers
	[n, d] = size(X);

	% check how many classes
	uniqueClasses = unique(Y);
	numClasses = length(uniqueClasses);

	if numClasses>2 || numClasses<2
		error('analysis only works for binary classes')
	end

	% split into positive and negative instances
	posIdx = allY==1;
	negIdx = allY==-1;
	Xpos = X(posIdx,:);
	Xneg = X(negIdx,:);

	% initialize results table
	table_out = zeros(d, 3);

	% populate results table
	% for each feature
	for i=1:d

		% get positive and negative examples
		posExamples = xPos(:,i);
		negExamples = xNeg(:,i);

		% perform Bonferroni corrected 
		[p,h] = ranksum(posExamples, negExamples,'alpha',0.05/d);

		% populate table
		table_out(i,1) = i;
		table_out(i,2) = p;
		table_out(i,3) = h;
	end
	
end