function [featVector_out] = computeStatsSession(featMat, flow_struct)

% get mode feats
modeFeats = mode(featMat(:,1:5));

% get statistics features
otherFeats = featMat(:,6:end);
maxFeats = nanmax(otherFeats);
minFeats = nanmin(otherFeats);
meanFeats = nanmean(otherFeats);

% set output
featVector_out = [modeFeats, maxFeats, minFeats, meanFeats];
