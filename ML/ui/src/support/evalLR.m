function resultsOut = evalLR(Xtr, Ytr, Xte, Yte)

	% initialize results
	results = cell(1, 4);

	% scale features
	[Xtr, Xte] = scaleFeats(Xtr, Xte);

	% perform CV
	options = sprintf('-s 0 -C');
	model = train(Ytr, sparse(Xtr), options);

	% get CV results
	optimalC = model(2);

	% train with optimal C
	options = sprintf('-s 0 -q -c %0.9f', optimalC);
	model = train(Ytr, sparse(Xtr), options);

	% predict
	[~, ~, dec_values] = predict(Yte, sparse(Xte), model,'-b 1 -q');

	% get positive values
	posPreds = dec_values(:,(model.Label(1)==0)+1);

	% compute AUROC
	[a,b,~,AUROC] = perfcurve(Yte, posPreds, 1);

	% check if < 0.5
	if AUROC<=0.5
		fprintf('****** flipping\n')
		[a,b,~,AUROC] = perfcurve(Yte, posPreds, 0);
	end

	% save everything	
	results{1} = AUROC;
	results{2} = a;
	results{3} = b;
	results{4} = model;

	% set output
	resultsOut = results;

end

