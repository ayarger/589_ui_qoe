clear; clc;

% read instances files
X = csvread('../data/instances.txt');
Y = csvread('../data/labels.txt');

% get sizes
[n, d] = size(X);

% split into 70/30
c = cvpartition(Y, 'holdout',0.3);

% get training and testing
Xtr = X(c.training);
Ytr = Y(c.training);

Xte = X(c.test);
Yte = Y(c.test);

% run learning algorithms
results_LR = evalLR(Xtr, Ytr, Xte, Yte);
results_RF = evalRF(Xtr, Ytr, Xte, Yte);

% save results
save('../data/results.mat', 'results_LR', 'results_RF');
