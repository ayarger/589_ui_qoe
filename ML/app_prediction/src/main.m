clear; clc;

% read data
load('../data/features.mat');

% get sizes
[n, d] = size(X);

% print ratios
uniqueIDs = unique(Y);
fprintf('*** printing ratios \n')
for i=1:length(uniqueIDs)
	fprintf('****** ratio of class %2d: %f \n', uniqueIDs(i), sum(Y==uniqueIDs(i))/n);
end

% perform split
fprintf('*** splitting data \n')
c = cvpartition(Y,'holdout',0.2);

% get training and testing
Xtr = X(c.training,:);
Ytr = Y(c.training);

Xte = X(c.test,:);
Yte = Y(c.test);

% run learning algorithms
results_LR = evalLR(Xtr, Ytr, Xte, Yte);
results_RF = evalRF(Xtr, Ytr, Xte, Yte);

% evaluate each class individually
save('../data/results.mat', 'results_LR', 'results_RF');