function resultsOut = evalLR(Xtr, Ytr, Xte, Yte)

	% scale features
	[Xtr, Xte] = scaleFeats(Xtr, Xte);

	% get unique labels
	uniqueTrain = unique(Ytr);
	uniqueTest = unique(Yte);

	% sanity check
	assert(length(uniqueTrain)==length(uniqueTest))

	% initialize cell to hold results
	results = cell(length(uniqueTrain)-1, 5);

	% for each unique label
	for i=1:length(uniqueTrain)-1

		% print
		fprintf('*** label %d/%d\n', i, length(uniqueTrain)-1)

		% set new labels
		Ytr_new = (Ytr==uniqueTrain(i))*1;
		Yte_new = (Yte==uniqueTrain(i))*1;

		% perform CV
		options = sprintf('-s 0 -C');
		model = train(Ytr_new, sparse(Xtr), options);

		% get CV results
		optimalC = model(2);

		% train with optimal C
		options = sprintf('-s 0 -q -c %0.9f', optimalC);
		model = train(Ytr_new, sparse(Xtr), options);

		% predict
		[~, ~, dec_values] = predict(Yte_new, sparse(Xte), model,'-b 1 -q');

		% get positive values
		posPreds = dec_values(:,(model.Label(1)==0)+1);

		% compute AUROC
		[a,b,~,AUROC] = perfcurve(Yte_new, posPreds, 1);

		% check if < 0.5
		if AUROC<=0.5
			fprintf('****** flipping\n')
			[a,b,~,AUROC] = perfcurve(Yte_new, posPreds, 0);
		end

		% save everything
		results{i,1} = uniqueTrain(i);
		results{i,2} = AUROC;
		results{i,3} = a;
		results{i,4} = b;
		results{i,5} = model;

	end

	% set output
	resultsOut = results;

end

