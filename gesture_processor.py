import sys
import math

def contains_gesture(gesture, ui_file):
    if gesture is None:
	raise Exception("Submitted a None gesture.")

    # The following code adapted from ui_playback_utility's simulation_controller.cs.
    horizontal_val = -1;
    vertical_val = -1;

    ready_horizontal = False
    ready_vertical = False
    currently_released = True

    for line in open(ui_file, "rb"):
	tokens = line.split()
	num_tokens = len(tokens)
	
	# Release command.
	if num_tokens == 3:
	    # Error checking
	    #if currently_released == True:
		#print "ERROR: Encountered a release line while already released."

	    if gesture.on_release(float(tokens[0])):
		return True
	    ready_horizontal = False
	    ready_vertical = False
	    currently_released = True

	# Press command.
	elif num_tokens == 5:
	    interaction_time = float(tokens[0])
	    interaction_type = tokens[3]
	    interaction_position = float(tokens[4])

	    # Take note of the collected horizontal or vertical value.
	    if interaction_type == "Horizontal":
		horizontal_val = interaction_position
	    elif interaction_type == "Vertical":
		vertical_val = interaction_position
	    else:
		raise Exception("New interaction type discovered: " + str(interaction_type))

	    # Update the state machine to decide if we have a new point or not.
	    if (ready_vertical and interaction_type == "Horizontal") or (ready_horizontal and interaction_type == "Vertical"):
		ready_horizontal = False
		ready_vertical = False
		new_tap = (horizontal_val, vertical_val, interaction_time)

		if currently_released == True:
		    currently_released = False
		    if gesture.on_tap(new_tap):
			return True
		else:
		    if gesture.on_drag(new_tap):
			return True
	    else:
		if interaction_type == "Horizontal":
		    ready_horizontal = True
		elif interaction_type == "Vertical":
		    ready_vertical = True
		else:
		    print "Error: New interaction type discovered: " + str(interaction_type)
		    raise Exception("New interaction type discovered:" + str(interaction_type))
	elif num_tokens == 4:
	    pass
	else:
	    if not "Start" in line and not "Finished" in line:
		raise Exception("Found new UI-input line format size:\n" + str(line) + "\nof size: " + str(num_tokens) + '\nin file: ' + str(ui_file))

    return False

class rolling_list():
    def __init__(self, max_capacity):
	self._max_capacity = max_capacity;
	self.internal_list = [];

    def append(self, new_item):
	"""
	Append an item to the rolling list. If the list reaches capacity, remove the eldest element.
	"""
	if len(self.internal_list) < self._max_capacity:
	    self.internal_list.append(new_item)
	elif len(self.internal_list) == self._max_capacity:
	    del self.internal_list[0]
	    self.internal_list.append(new_item)

	# Error checking.
	if len(self.internal_list) > self._max_capacity:
	    raise Exception("Detected overflow of rolling list")

    def max_dist_from_center(self):
	avg_pos_x = 0
	avg_pos_y = 0
	for point in self.internal_list:
	    avg_pos_x += point[0]
	    avg_pos_y += point[1]
	    
	avg_pos_x /= self._max_capacity
	avg_pos_y /= self._max_capacity

	max_dist = 0
	for point in self.internal_list:
	    xdiff = abs(point[0] - avg_pos_x)
	    ydiff = abs(point[1] - avg_pos_y)
	    dist = math.sqrt(math.pow(xdiff, 2) + math.pow(ydiff, 2))
	    if dist > max_dist:
		max_dist = dist

	return max_dist

    def time_span(self):
	if len(self.internal_list) == 1:
	    return self.internal_list[2]
	
	early_time = 9999999999999999999999999
	late_time = 0
	
	for point in self.internal_list:
	    current_time = float(point[2])
	    if current_time > late_time:
		late_time = current_time
	    if current_time < early_time:
		early_time = current_time

	if early_time == late_time:
	    print "ERROR: early time equals the late time for some reason..."
	return late_time - early_time

class repeated_tap_gesture():
    def __init__(self, num_taps, max_distance, max_time_span):
	self._max_distance = max_distance
	self._max_time_span = max_time_span
	self._num_taps = num_taps
	self.previous_taps = rolling_list(self._num_taps)

    def on_tap(self, tap_data):
	# Note that a tap_data is a 3-tuple. (x, y, timestamp)
	# Error checking
	if tap_data[0] == -1 or tap_data[1] == -1 or tap_data[2] == -1:
	    raise Exception("Detected -1 in UI parsing.")
	
	self.previous_taps.append(tap_data)

	if len(self.previous_taps.internal_list) == 3:
	    #print "span: " + str(self.previous_taps.time_span())
	    #print "max dist from center: " + str(self.previous_taps.max_dist_from_center())

	    # Test if gesture has been detected.
	    if self.previous_taps.max_dist_from_center() <= self._max_distance and self.previous_taps.time_span() <= self._max_time_span:
		return True

	return False
    
    def on_drag(self, drag_data):
	# Note that a tap_data is a 3-tuple. (x, y, timestamp)
	return False

    def on_release(self, timestamp):
	return False 
    
class quadrant_popularity():
    def __init__(self, resolution_hor, resolution_vert):
	self.quad_times = [0.0, 0.0, 0.0, 0.0]
	self._resolution_hor = resolution_hor
	self._resolution_vert = resolution_vert
	self.previous_data = None

    def on_tap(self, tap_data):
	self.previous_data = tap_data
	return False

    def on_drag(self, tap_data):
    	time_diff = tap_data[2] - self.previous_data[2]
	# Determine which quadrant this time took place in.
	# For now, just consider the quad we end in.
	quad_index = self.get_quad_index(tap_data)
	self.quad_times[quad_index] += time_diff

	return False

    def on_release(self, timestamp):
	return False	
	
    def get_quad_index(self, point_data):
	x = point_data[0]
	y = point_data[1]

	if x < self._resolution_hor * 0.5:
	    if y < self._resolution_vert * 0.5:
		return 2
	    else:
		return 0	
	else:
	    if y < self._resolution_vert * 0.5:
		return 3
	    else:
		return 1
	
	raise Exception("Somehow missed every conditional in get_quad_index().")
	return -1

    def __str__(self):
	return str(self.quad_times)

class long_press_gesture():
    def __init__(self, max_distance, max_time):
	self.previous_tap = rolling_list(1)
	self.max_distance = max_distance
	self.max_time = max_time
	self.dist = 0
    def on_tap(self, tap_data):
	# Note that a tap_data is a 3-tuple. (x, y, timestamp)
	# Error checking
	if tap_data == -1:
	    print "ERROR: detected -1 in UI parsing."
	    sys.exit(1)
	
	self.previous_tap.append(tap_data)
	return False

    def on_drag(self, drag_data):
	# Note that a tap_data is a 3-tuple. (x, y, timestamp)
	#calculate distance 	
	xdiff = abs(drag_data[0]- float(self.previous_tap.internal_list[0][0]))
	ydiff = abs(drag_data[1] - float(self.previous_tap.internal_list[0][1]))
	self.dist = math.sqrt(math.pow(xdiff, 2) + math.pow(ydiff, 2))
	return False

    def on_release(self, timestamp):
    	duration = timestamp - float(self.previous_tap.internal_list[0][2])
    	if (self.dist < self.max_distance and duration > self.max_time):
		return True 
				 
        #print "DISTANCE is " + str(self.dist)
        #print "Long press detected"
        #print "Duration is " + str (duration)
        return False
