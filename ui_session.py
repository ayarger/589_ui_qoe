class ui_session:
    def __init__(self):
	self.user_id = -1
	self.start_time = -1
	self.end_time = -1
	
	# A list of files containing UI data relevant to this session.
	self.ui_data = []
	self.detected_triple_tap_gesture = None
	self.popular_quadrant = None
	self.detected_long_press_gesture = None
	# A list of files containing pcaps relevant to this session.
    	self.flows = []

    def __str__(self):
	ui_s = ""
	for ui in self.ui_data:
	    ui_s += ui + '\n'

	flow_s = ""
	for flow in self.flows:
	    flow_s += "BEGIN FLOW\n" + '\n'.join(flow) + '\n'

	s = "start_time\n" + str(self.start_time) + '\n' + "end_time\n" + str(self.end_time) + "\nuser\n" + str(self.user_id) + '\ndetected triple tap gesture:\n' + str(self.detected_triple_tap_gesture) + '\nflows ' + str(len(self.flows)) + '\n' + flow_s
	return s
